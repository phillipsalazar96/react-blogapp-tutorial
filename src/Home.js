
import BlogList from './BlogList';
import useFetch from './useFetch';

const Home = () => {


    const {data:blogs, isPending, error} = useFetch('http://localhost:8000/blogs');

    const title = "ALL Blog Posts";

    return (
        <div className="home">
            {error && <div>{error}</div>}
            {isPending && <div>Ladoing...</div>}
            {blogs && <BlogList blogs={blogs} title={title} />}
        </div>
    );
}
export default Home;